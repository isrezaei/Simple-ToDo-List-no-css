let D = document

const MyInput = D.querySelector('.MyInputs')
const MySubmit = D.querySelector('.MySubmit')
const MyOption = D.querySelector('.MyOption')
const UlParent = D.querySelector('.UlParent')


MySubmit.addEventListener('click', CreateElement)
UlParent.addEventListener('click', Remove_Done)
UlParent.addEventListener('click', ChangeTask)
MyOption.addEventListener('click', SwitchItem)
document.addEventListener('DOMContentLoaded', ReloadData)




function CreateElement() {

    SavceLocal(MyInput.value)

    const C_div = D.createElement('div')
    const C_li = D.createElement('li')


    const C_Task = document.createElement('span')
    C_Task.innerText = MyInput.value

    const C_Remove_BTN = D.createElement('button')
    C_Remove_BTN.innerText = 'Remove Task'
    C_Remove_BTN.classList.add('RemoveBTN')

    const C_Done_BTN = D.createElement('button')
    C_Done_BTN.innerText = 'Done Task'
    C_Done_BTN.classList.add('DoneBTN')

    const C_Change_BTN = D.createElement('button')
    C_Change_BTN.innerText = 'Change Task'
    C_Change_BTN.classList.add('ChangeBTN')

    C_li.appendChild(C_Task)
    C_li.appendChild(C_Done_BTN)
    C_li.appendChild(C_Remove_BTN)
    C_li.appendChild(C_Change_BTN)


    C_div.appendChild(C_li)
    UlParent.appendChild(C_li)

    MyInput.value = ""

}


function Remove_Done(event) {
    const Event = event.target // Each Element in ul
    const ParentEventTarget = Event.parentElement //Parent of Each child in li

    if (Event.classList[0] === 'RemoveBTN') {
        RemoveLocal(ParentEventTarget)
        ParentEventTarget.remove() //RemoveBTN Parent == li
    }

    if (Event.classList[0] === 'DoneBTN') {
        ParentEventTarget.classList.toggle('DoneTask')
    }
}




function SwitchItem(event) {

    const UlTask = UlParent.childNodes

    UlTask.forEach(value => {

        switch (event.target.value) {

            case 'AllTask':
                value.style.display = 'flex'
                break

            case 'Complited':
                if (value.classList == 'DoneTask') {
                    value.style.display = 'flex'
                } else {
                    value.style.display = 'none'
                }
                break

            case 'UnComplited':
                if (value.classList == 'DoneTask') {
                    value.style.display = 'none'
                } else {
                    value.style.display = 'block'
                }
                break
        }
    })
}


function ChangeTask(event) {
    const UL = UlParent.childNodes
    const ChangeBTN = event.target //Childern in li
    const Parent = ChangeBTN.parentElement // Parent Eaech Element in li == li

    if (ChangeBTN.classList[0] === 'ChangeBTN') {
        Parent.children[0].innerHTML = prompt('Edit Your Task') // == Text li
    }

    UL.forEach(value => {
        ChangeLocal(value.children[0].innerText)
    })

}


function SavceLocal(Task) {
    let NewTask;

    if (localStorage.getItem('MyTask') === null) {
        NewTask = []
    } else {
        NewTask = JSON.parse(localStorage.getItem('MyTask'))
    }

    NewTask.push(Task)
    localStorage.setItem('MyTask', JSON.stringify(NewTask))
}


function ChangeLocal(Task) {
    let ChangeTask;
    if (localStorage.getItem('MyTask') == null) {
        ChangeTask = []
    } else {
        ChangeTask = JSON.parse(localStorage.getItem('MyTask'))
    }


    ChangeTask.splice(ChangeTask, 1) // All Arry of Local - 1


    ChangeTask.push(Task) // Push Task index to Remove Array

    localStorage.setItem('MyTask', JSON.stringify(ChangeTask))



}
 


function RemoveLocal(Task) {
    let NewTask;

    if (localStorage.getItem('MyTask') === null) {
        NewTask = []
    } else {
        NewTask = JSON.parse(localStorage.getItem('MyTask'))
    }

    const Delete = Task.children[0].innerHTML // Childe li == Text

    NewTask.splice(NewTask.indexOf(Delete), 1)

    localStorage.setItem('MyTask', JSON.stringify(NewTask))


    function ReloadData() {

        let NewTask;

        if (localStorage.getItem('MyTask') === null) {
            NewTask = []
        } else {
            NewTask = JSON.parse(localStorage.getItem('MyTask'))
        }

        NewTask.forEach(value => {

            const C_div = D.createElement('div')
            const C_li = D.createElement('li')


            const C_Task = document.createElement('span')
            C_Task.innerText = value

            const C_Remove_BTN = D.createElement('button')
            C_Remove_BTN.innerText = 'Remove Task'
            C_Remove_BTN.classList.add('RemoveBTN')

            const C_Done_BTN = D.createElement('button')
            C_Done_BTN.innerText = 'Done Task'
            C_Done_BTN.classList.add('DoneBTN')

            const C_Change_BTN = D.createElement('button')
            C_Change_BTN.innerText = 'Change Task'
            C_Change_BTN.classList.add('ChangeBTN')


            C_li.appendChild(C_Task)
            C_li.appendChild(C_Done_BTN)
            C_li.appendChild(C_Remove_BTN)
            C_li.appendChild(C_Change_BTN)

            C_div.appendChild(C_li)
            UlParent.appendChild(C_li)

            MyInput.value = ""


        })


    }
}